import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductComponent } from './demos/product/product.component';
import { BuyFormComponent } from './demos/product/buy-form/buy-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShoppingPageComponent } from './demos/shopping-page/shopping-page.component';
import { ItemInfoListComponent } from './demos/shopping-page/item-info-list/item-info-list.component';
import { ShoppingCartComponent } from './demos/shopping-cart/shopping-cart.component';
import { CartItemListComponent } from './demos/shopping-cart/cart-item-list/cart-item-list.component';
import { MySumPipe } from './demos/shopping-cart/my-sum.pipe';
import { ProductService } from './demos/Shared/Services/product.service';
import { HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './demos/login/login.component';
import { LogoutComponent } from './demos/logout/logout.component';
import { RegisterComponent } from './demos/register/register.component';
import { AuthService } from './demos/Shared/Services/auth.service';
import { OrderComponent } from './demos/order/order.component';
import { OrderItemListComponent } from './demos/order/order-item-list/order-item-list.component';
import { AdminDashboardComponent } from './demos/admin-dashboard/admin-dashboard.component';
import { CheckOrderComponent } from './demos/admin-dashboard/check-order/check-order.component';
import { DeleteProductComponent } from './demos/admin-dashboard/delete-product/delete-product.component';
import { EditProductComponent } from './demos/admin-dashboard/edit-product/edit-product.component';
import { AddProductComponent } from './demos/admin-dashboard/add-product/add-product.component';
import { AdminService } from './demos/Shared/Services/admin.service';
import { OrderBoardComponent } from './demos/admin-dashboard/check-order/order-board/order-board.component';
import { AppGuard } from './app.guard';




@NgModule({
  declarations: [
    AppComponent,
    BuyFormComponent,
    ShoppingPageComponent,
    ItemInfoListComponent,
    ShoppingCartComponent,
    CartItemListComponent,
    MySumPipe,
    ProductComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    OrderComponent,
    OrderItemListComponent,
    AdminDashboardComponent,
    CheckOrderComponent,
    DeleteProductComponent,
    EditProductComponent,
    AddProductComponent,
    OrderBoardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ProductService, AuthService, AdminService, AppGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
