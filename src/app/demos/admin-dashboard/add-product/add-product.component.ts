import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../Shared/Services/admin.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  constructor(private adminService: AdminService) { }

  ngOnInit(): void {
  }

  addProduct(product:any){
    this.adminService.addProduct(product)
    .subscribe((res) =>{
      
    });
  }

}
