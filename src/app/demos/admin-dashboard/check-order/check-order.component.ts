import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import { AdminService } from '../../Shared/Services/admin.service';
import { ProductService } from '../../Shared/Services/product.service';

@Component({
  selector: 'app-check-order',
  templateUrl: './check-order.component.html',
  styleUrls: ['./check-order.component.css']
})
export class CheckOrderComponent implements OnInit {

  uProductFormGroup: FormGroup;
  userNames: any;
  wantUsername: String = "";
  curName: any;
  orders: any;

  constructor(private fb: FormBuilder, private adminService: AdminService, private productService: ProductService) {
    this.uProductFormGroup = this.fb.group({
      user:[null, [Validators.required]],
    });
   }

  ngOnInit(): void {
    this.retrieveUsernames();
  }

  getOrderByName(){
    this.wantUsername = this.curName;
    this.adminService.getOrderByUsername(this.uProductFormGroup.get('user')?.value)
    .subscribe(
      data => {
      this.orders = data;
      console.log(data);
    },
    error => {
      console.log(error);
    }
    );
  }

  updateName(event: any): void{
    this.curName = event.target.options[event.target.options.selectedIndex].text;
    console.log(this.curName);
  }

  retrieveUsernames():void{
    this.adminService.getUsernames()
    .subscribe(
      data => {
      this.userNames = data;
      console.log(data);
    },
    error => {
      console.log(error);
    }
    );
  }

  getAllOrder(){
    this.adminService.getAllOrder()
    .subscribe(
      data => {
      this.orders = data;
      console.log(data);
    },
    error => {
      console.log(error);
    }
    );
  }

  // getOrderByName(event:any){
  //   this.adminService.getOrderByUsername()
  //   .subscribe(
  //     data => {
  //     this.userNames = data;
  //     console.log(data);
  //   },
  //   error => {
  //     console.log(error);
  //   }
  //   );
  // }

}
