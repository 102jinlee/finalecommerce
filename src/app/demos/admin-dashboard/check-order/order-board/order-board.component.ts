import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-board',
  templateUrl: './order-board.component.html',
  styleUrls: ['./order-board.component.css']
})
export class OrderBoardComponent implements OnInit {

  @Input()
  Order: any;

  constructor() { }

  ngOnInit(): void {
  }

}
