import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import { AdminService } from '../../Shared/Services/admin.service';
import { ProductService } from '../../Shared/Services/product.service';
@Component({
  selector: 'app-delete-product',
  templateUrl: './delete-product.component.html',
  styleUrls: ['./delete-product.component.css']
})
export class DeleteProductComponent implements OnInit {

  dProductFormGroup: FormGroup;

  curName: any; 

  // productName = [{"id":1, "name":"a"},
  // {"id":2, "name":"b"},
  // {"id":3, "name":"c"},
  // {"id":88, "name":"d"},
  // {"id":90, "name":"e"}
  // ];
  productName : any;


  constructor(private fb: FormBuilder, private adminService: AdminService, private productService: ProductService) { 
    this.dProductFormGroup = this.fb.group({
      products:[null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.retrieveProducts();
  }

  onSubmit(){
    // console.log(this.dProductFormGroup.value);
    // console.log(this.dProductFormGroup.get('products')?.value);
    // console.log(this.dProductFormGroup.controls['products']?.value);
    this.adminService.deleteProduct(this.dProductFormGroup.get('products')?.value)
    .subscribe((res) =>{
      
    });
  }

  updateName(event: any): void{
    this.curName = event.target.options[event.target.options.selectedIndex].text;
    console.log(this.curName);
  }

  retrieveProducts():void{
    this.productService.getAllNameId()
    .subscribe(
      data => {
      this.productName = data;
      console.log(data);
    },
    error => {
      console.log(error);
    }
    );
  }

}
