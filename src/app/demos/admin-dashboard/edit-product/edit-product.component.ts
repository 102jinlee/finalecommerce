import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import { AdminService } from '../../Shared/Services/admin.service';
import { ProductService } from '../../Shared/Services/product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  cProductFormGroup: FormGroup;

  curName: any; 
  wantName: String = "";
  wantProduct: any;

  productName : any;

  constructor(private fb: FormBuilder, private adminService: AdminService, private productService: ProductService) { 
    this.cProductFormGroup = this.fb.group({
      products:[null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.retrieveProducts();
  }

  editProduct(product:any){
    product["id"] = this.wantProduct["id"];
    console.log(product);
    this.adminService.editProduct(product)
    .subscribe((res) =>{
      
    });
    
  }

  nameSubmit(){
    this.wantName = this.curName;
    this.productService.getById(this.cProductFormGroup.get('products')?.value)
    .subscribe(
      data => {
      this.wantProduct = data;
      console.log(data);
    },
    error => {
      console.log(error);
    }
    );
  }

  updateName(event: any): void{
    this.curName = event.target.options[event.target.options.selectedIndex].text;
    console.log(this.curName);
  }

  retrieveProducts():void{
    this.productService.getAllNameId()
    .subscribe(
      data => {
      this.productName = data;
      console.log(data);
    },
    error => {
      console.log(error);
    }
    );
  }
}
