import { Component, OnInit } from '@angular/core';
import { Subscription, VirtualTimeScheduler, forkJoin } from 'rxjs';
import { AdminService } from '../Shared/Services/admin.service';
import { AuthService } from '../Shared/Services/auth.service';
import { SingleOrderService } from '../Shared/Services/single-order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  protected orderSub: Subscription = new Subscription();
  protected cartItemSub: Subscription = new Subscription();
  user: any;
  order: any;
  cartItems: any;

  items = [
    {name: 'Strawberry', url:'https://images.everydayhealth.com/images/ordinary-fruits-with-amazing-health-benefits-05-1440x810.jpg', price: 1.2},
    {name:'Ara Fruit', url:'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/heap-of-pomegranate-fruit-royalty-free-image-1618603844.',price:3.6},
    {name:'Blackberries', url:'https://cdn.shopify.com/s/files/1/0059/8835/2052/products/Thornless_Blackberry_3_650x.jpg?v=1612444137', price:3.7},
    {name:'Red Seedless Grape', url:'https://images.albertsons-media.com/is/image/ABS/184100012?$ng-ecom-pdp-desktop$&defaultImage=Not_Available', price:5.9},
    {name:'Star Fruit',url:'https://images.heb.com/is/image/HEBGrocery/000377519',price:4.2},
    {name:'Pineapple', url:'https://images.unsplash.com/photo-1550258987-190a2d41a8ba?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&w=1000&q=80',price:1.8},
    {name:'Watermelon', url:'https://static.onecms.io/wp-content/uploads/sites/44/2022/12/21/bad-fruits-you-should-be-eating.jpg', price:3.7},
    {name:'Papaya', url:'https://cdn.shopify.com/s/files/1/0336/7167/5948/products/4-count-image-of-strawberry-papaya-fruit-29921774338092_512x512.jpg?v=1648081939',price:3.8},
    {name:'Banana', url:'https://www.verywellfit.com/thmb/qeIiD7JDWsOr4_Ymg4GCaxhhOZs=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/Bananas-5c6a36a346e0fb0001f0e4a3.jpg',price:0.8},
    {name:'Dragon Fruit',url:'https://www.foodsafetynews.com/files/2022/01/dreamstime_dragon-fruit.jpg',price:4.8}
  ];

  constructor(private singleOrderService: SingleOrderService, private authService: AuthService) {
   }

  ngOnInit(): void {
    this.user = {"name":"Chi", "address":"Texas", "payment":"Amex"}
    this.orderSub = this.singleOrderService.getProductOrder()
    .subscribe(order => {
        this.order = order;
        console.log(order);
    });
    this.cartItemSub = this.singleOrderService.getCartSource()
    .subscribe(cartItem =>{
      this.cartItems = cartItem;
      console.log(cartItem);
    });
    console.log(this.cartItems);
    this.retrieveUsername();
  }

  ngOnDestroy() {
    if(this.orderSub) {
      this.orderSub.unsubscribe();
    }
    if(this.cartItemSub){
      this.cartItemSub.unsubscribe();
    }
  }

  retrieveUsername():void{
    this.authService.getSingleUser()
    .subscribe(
      data => {
      this.user = data;
      console.log(data);
    },
    error => {
      console.log(error);
    }
    );
  }

  delieverOrder(){
    this.singleOrderService.purchaseOrder(this.order)
    .subscribe((res => { 
    }));
  }

  delCartItems(){
    let wrapCartItems = {"caritems":this.cartItems};
    this.singleOrderService.deleteCartItems(wrapCartItems)
    .subscribe((res => { 
    }));
  }

  // submitOrder(){
  //   forkJoin([this.delieverOrder,this.delCartItems])
  //   .subscribe((res => { 
  //   }));
  // }

  // submitOrder(){
  //   this.singleOrderService.deleteCartItems(this.cartItems)
  //   .subscribe((res => { 
  //   }));
  // }

  submitOrder(){
    this.singleOrderService.purchaseOrder(this.order)
    .subscribe((res => { 
    }));
  }

  // submitOrder(){
  //   this.singleOrderService.purchaseOp(this.order, this.cartItems)
  //   .subscribe((res => { 
  //   }));
  // }

}
