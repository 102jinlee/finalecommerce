import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-item-list',
  templateUrl: './order-item-list.component.html',
  styleUrls: ['./order-item-list.component.css']
})
export class OrderItemListComponent implements OnInit {

  @Input()
  item: any;

  @Input()
  index: number = 0;
  
  qty = 1;
  
  constructor() { }

  ngOnInit(): void {
  }

}
