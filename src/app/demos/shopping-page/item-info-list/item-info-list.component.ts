import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { shoppageItems } from '../../Shared/Models/shoppageItems';
import { ProductService } from '../../Shared/Services/product.service';

@Component({
  selector: 'app-item-info-list',
  templateUrl: './item-info-list.component.html',
  styleUrls: ['./item-info-list.component.css']
})
export class ItemInfoListComponent implements OnInit {

  @Input()
  product: any;

  @Input()
  index: number = 0;

  constructor(private productService: ProductService, private router: Router) { }

  ngOnInit(): void {
  }

  clickProduct(){
    // this.productService.changeProduct(this.productService.getById(this.product.id));
    // this.productService.changeProduct(this.product);
    this.router.navigate([`/product/${this.product.id}`]);
  }

  // retrieveProducts():void{
  //   this.productService.getAll()
  //   .subscribe(
  //     data => {
  //     this.product = data;
  //     console.log(data);
  //   },
  //   error => {
  //     console.log(error);
  //   }
  //   );
  // }

}
