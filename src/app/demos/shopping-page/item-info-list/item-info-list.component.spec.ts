import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemInfoListComponent } from './item-info-list.component';

describe('ItemInfoListComponent', () => {
  let component: ItemInfoListComponent;
  let fixture: ComponentFixture<ItemInfoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemInfoListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemInfoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
