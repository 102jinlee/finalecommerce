import { Component, OnInit } from '@angular/core';
import { ProductService } from '../Shared/Services/product.service';

@Component({
  selector: 'app-shopping-page',
  templateUrl: './shopping-page.component.html',
  styleUrls: ['./shopping-page.component.css']
})
export class ShoppingPageComponent implements OnInit {

  products: any;

  page = 0;
  
  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.retrieveProducts();
  }

  retrieveProducts():void{
    this.productService.getAll()
    .subscribe(
      data => {
      this.products = data;
      console.log(data);
    },
    error => {
      console.log(error);
    }
    );
  }

}
