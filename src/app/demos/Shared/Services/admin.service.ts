//@ts-nocheck
import { Injectable } from '@angular/core';
import { AppConfig } from './app.config';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';
import { BehaviorSubject, Observable, Subject, pipe } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private API_URL = AppConfig.API_URL;

  constructor(private http: HttpClient, private router: Router) { }

  addProduct(product:any): Observable<any>{ //pass json, 
    return this.http.post(this.API_URL + "/admin", product)
      .pipe(map(res => {
        console.log(res);
        if(res.success){
          console.log("Add Product Success")
          this.router.navigate(['/manageDashboard']);
        }
      }));
  }

  editProduct(product:any): Observable<any>{ //pass json, 
    return this.http.put(this.API_URL + "/admin", product)
      .pipe(map(res => {
        console.log(res);
        if(res.success){
          console.log("Edit Product Success")
          this.router.navigate(['/manageDashboard']);
        }
      }));
  }

  
  getUsernames(): Observable<any>{
    return this.http.get(this.API_URL + "/admin/usernames");
  }

  getOrderByUsername(username:String): Observable<any>{
    return this.http.get(this.API_URL + "/admin/orders/" + username);
  }

  getAllOrder(): Observable<any>{
    return this.http.get(this.API_URL + "/admin/orders");
  }

  deleteProduct(id): Observable<any>{ 
    id = id.toString();
    console.log(this.API_URL + "/admin/" + id);
    return this.http.delete(this.API_URL + "/admin/" + id)
      .pipe(map(res => {
        console.log(res);
        if(res.success){
          console.log("delete Product Success")
          this.router.navigate(['/manageDashboard']);
        }
      }));
  }
}
