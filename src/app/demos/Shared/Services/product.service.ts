//@ts-nocheck
import { Injectable } from '@angular/core';
import { AppConfig } from './app.config';
//import {Http, URLSearchParams} from '@angular/http';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import { BehaviorSubject, Observable, Subject, pipe, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private API_URL = AppConfig.API_URL;
  private productSource = new BehaviorSubject<any>();
  curProduct = this.productSource.asObservable();
  

  constructor(private http: HttpClient, private router: Router) { }

  getAll(): Observable<any>{
    console.log(this.API_URL + "/products");
    return this.http.get(this.API_URL + "/products");
    // .pipe(map(res => {
    //   console.log(res);
    //   if(res.success){
    //     console.log(success);
    //   }
    // }));
  }

  getById(id): Observable<any>{
    id = id.toString();
    // return this.http.get(`${this.API_URL}/products/${id}`);
    // console.log(this.http.get(this.API_URL + "/products/" + id));
    return this.http.get(this.API_URL + "/products/" + id);
  }

  getAllNameId(): Observable<any>{
    return this.http.get(this.API_URL + "/products/IdName");
  }

  changeProduct(product: any){
    this.productSource.next(product);
  }

  addProductToCart(product: any, qty:number): Observable<any>{
    console.log(product);
    console.log(typeof product);
    console.log(qty);
    qty = qty.toString();
    return this.http.post(this.API_URL + "/products/addtocart/" + qty, product, {withCredentials: true})
    .pipe(map((res) => {
      console.log(res);
      if(res.success){
        console.log("Add to cart Success")
        this.router.navigate(['/shopcart']);
      }
      else{
        console.log("Add to cart Fail")
      }
    }));
  }

  getShopcartItems(): Observable<any>{
    return this.http.get(this.API_URL + "/products/shopcart", {withCredentials: true});
  }

}
