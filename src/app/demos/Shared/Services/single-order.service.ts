//@ts-nocheck
import { Injectable } from '@angular/core';
import { AppConfig } from './app.config';
//import {Http, URLSearchParams} from '@angular/http';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import { BehaviorSubject, Observable, Subject, pipe, Subscription, forkJoin } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SingleOrderService {

  private API_URL = AppConfig.API_URL;
  private productSource = new BehaviorSubject<any>(null);
  private cartSource = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient, private router: Router) { }

  singleProductOrder(order:any){
    this.productSource.next(order);
    this.router.navigate(['/order']);
  }

  getProductOrder(): Observable<any>{
    return this.productSource.asObservable();
  }

  setCartSource(cartItems:any){
    this.cartSource.next(cartItems);
  }

  getCartSource(): Observable<any>{
    return this.cartSource.asObservable();
  }

  purchaseOrder(order:any): Observable<any>{ //pass json, 
    return this.http.post(this.API_URL + "/order", order, {withCredentials: true})
      .pipe(map(res => {
        console.log(res);
        if(res.success){
          console.log("Purchase Order Success");
          this.router.navigate(['/shoppage']);
        }
        return res;
      }));
  }

  deleteCartItems(cartItems: any): Observable<any>{
    return this.http.delete(this.API_URL + "/products/deleteCart", cartItems, {withCredentials: true})
      .pipe(map(res => {
        console.log(res);
        if(res.success){
          console.log("delete cart items successfully");
        }
        return res;
      }))
  }

  purchaseOp(order:any, cartItems: any): Observable<any>{
    const purchase = this.http.post(this.API_URL + "/order", order, {withCredentials: true})
    .pipe(map(res => res.json()));
    const delCart = this.http.delete(this.API_URL + "/products/deleteCart", cartItems, {withCredentials: true})
    .pipe(map(res => res.json()));

    return forkJoin([purchase, delCart])
    .pipe(map(res => {
        console.log(res);
        if(res.success){
          console.log("Purchase Order Success")
          console.log("delete cart items successfully");
        }
    }))
  }
}
