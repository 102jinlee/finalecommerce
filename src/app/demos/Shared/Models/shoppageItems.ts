export class shoppageItems{
    name: string;
    url: string;
    price: number;

    constructor(name: string, url: string, price: number){
        this.name = name;
        this.url = url;
        this.price = price;
    }
}