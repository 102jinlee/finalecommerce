import { Component, OnInit,  ViewChildren, QueryList} from '@angular/core';
import { ProductService } from '../Shared/Services/product.service';
import {map} from 'rxjs/operators';
import { CartItemListComponent } from './cart-item-list/cart-item-list.component';
import { SingleOrderService } from '../Shared/Services/single-order.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  @ViewChildren(CartItemListComponent) childrenlist! : QueryList<CartItemListComponent>;
  // items = [
  //   {name: 'Strawberry', url:'https://images.everydayhealth.com/images/ordinary-fruits-with-amazing-health-benefits-05-1440x810.jpg', price: 1.2},
  //   {name:'Ara Fruit', url:'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/heap-of-pomegranate-fruit-royalty-free-image-1618603844.',price:3.6},
  //   {name:'Blackberries', url:'https://cdn.shopify.com/s/files/1/0059/8835/2052/products/Thornless_Blackberry_3_650x.jpg?v=1612444137', price:3.7},
  //   {name:'Red Seedless Grape', url:'https://images.albertsons-media.com/is/image/ABS/184100012?$ng-ecom-pdp-desktop$&defaultImage=Not_Available', price:5.9},
  //   {name:'Star Fruit',url:'https://images.heb.com/is/image/HEBGrocery/000377519',price:4.2},
  //   {name:'Pineapple', url:'https://images.unsplash.com/photo-1550258987-190a2d41a8ba?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&w=1000&q=80',price:1.8},
  //   {name:'Watermelon', url:'https://static.onecms.io/wp-content/uploads/sites/44/2022/12/21/bad-fruits-you-should-be-eating.jpg', price:3.7},
  //   {name:'Papaya', url:'https://cdn.shopify.com/s/files/1/0336/7167/5948/products/4-count-image-of-strawberry-papaya-fruit-29921774338092_512x512.jpg?v=1648081939',price:3.8},
  //   {name:'Banana', url:'https://www.verywellfit.com/thmb/qeIiD7JDWsOr4_Ymg4GCaxhhOZs=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/Bananas-5c6a36a346e0fb0001f0e4a3.jpg',price:0.8},
  //   {name:'Dragon Fruit',url:'https://www.foodsafetynews.com/files/2022/01/dreamstime_dragon-fruit.jpg',price:4.8}
  // ];

  items: any[] = new Array();
  prices: any[] = new Array();
  cartItems: any;
  
  constructor(private productService: ProductService, private singleOrderService: SingleOrderService) { }

  ngOnInit(): void {
    this.retrieveCartitems();
    console.log(this.prices);
  }

  ngAfterViewInit() {
    // this.getSelectProduct();
    
  }

  retrieveCartitems() : void{
    this.productService.getShopcartItems()
    .subscribe(
      data => {
      data.forEach((p:any) => {
        this.items.push({product:p['product'], qty:p['qty']});
        this.prices.push({price:p['product'].price*p['qty']});
      });
      this.cartItems = data;
      console.log(data);
    },
    error => {
      console.log(error);
    }
    );
  }

  getSelectProduct(): any{
    console.log(this.childrenlist);
    let order = {"total":0, "payment":"Amex", "orderItems":[] as any[]};
    // this.childrenlist.changes.subscribe(comp =>{
    //   console.log(comp);
    // });
    // this.order = {"total":this.money, "payment":"Amex",
    // "orderItems":[{"product":this.product, "qty":this.buyForm.get("quantity")?.value}]};

    // this.childrenlist
    // .filter(comp => comp.checked === true)
    // .forEach(comp => {
    //     order["total"] += comp.item["product"]["price"]*comp.item["qty"];
    //     order["orderItems"].push(comp.item);
    // });
    let postcartItems = [] as any[];
    this.childrenlist.map((x,index) => [x, this.cartItems[index]])
    .filter(comp => comp[0].checked === true)
    .forEach(comp => {
        order["total"] += comp[0].item["product"]["price"]*comp[0].item["qty"];
        order["orderItems"].push(comp[0].item);
        postcartItems.push(comp[1]);
    });
    console.log(order);
    console.log(postcartItems);
    this.singleOrderService.singleProductOrder(order);
    this.singleOrderService.setCartSource(postcartItems);
  }


}
