import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart-item-list',
  templateUrl: './cart-item-list.component.html',
  styleUrls: ['./cart-item-list.component.css']
})
export class CartItemListComponent implements OnInit {

  @Input()
  public item: any;

  public checked: boolean = false;
  

  constructor() { }

  ngOnInit(): void {
  }

  onCheckboxChange(event: any) {
    if (event.target.checked){
      this.checked = true;
    }
    else{
      this.checked = false;
    }
    console.log(this.checked);
  }

}
