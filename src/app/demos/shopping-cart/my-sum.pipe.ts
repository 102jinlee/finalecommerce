import { Pipe, PipeTransform } from '@angular/core';
import {shoppageItems} from '../Shared/Models/shoppageItems';

@Pipe({
  name: 'mySum'
})
export class MySumPipe implements PipeTransform {
  transform(items: any[], attr:string): number {
    return items.reduce((a,b) => a + b[attr] , 0)
  }

}
