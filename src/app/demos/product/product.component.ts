import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { ProductService } from '../Shared/Services/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  product: any;
  //@ts-ignore
  routeSub: Subscription;

  itemName = "North";
  itemDescript = "Harry Potter is a series of seven fantasy novels written by British author J. K. Rowling. The novels chronicle the lives of a young wizard, Harry Potter, and his friends Hermione Granger and Ron Weasley, all of whom are students at Hogwarts School of Witchcraft and Wizardry. The main story arc concerns Harry's struggle against Lord Voldemort, a dark wizard who intends to become immortal, overthrow the wizard governing body known as the Ministry of Magic and subjugate all wizards and Muggles (non-magical people).";
  
  constructor(private productService: ProductService, private route: ActivatedRoute) { }
  ngOnInit(): void {
    // this.routeSub = this.route.params.subscribe(params => {
    //   // console.log(params);
    //   // console.log(params['id']);
    //   this.retrieveProduct(params['id']);
    // });
    let id: any = this.route.snapshot.paramMap.get('id');
    if(id){
      this.retrieveProduct(id);
    }
  }

  // ngOnDestroy() {
  //   this.routeSub.unsubscribe();
  // }

  // retrieveProduct():void{
  //   this.productService.curProduct.subscribe(data => {
  //     this.product = data;
  //   });
  // }

  retrieveProduct(id: number):void{
    this.productService.getById(id)
    .subscribe(
      data => {
      this.product = data;
      console.log(data);
    },
    error => {
      console.log(error);
    }
    );
  }
}
