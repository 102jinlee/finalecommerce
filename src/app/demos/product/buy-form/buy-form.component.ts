import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductService } from '../../Shared/Services/product.service';
import { SingleOrderService } from '../../Shared/Services/single-order.service';

@Component({
  selector: 'app-buy-form',
  templateUrl: './buy-form.component.html',
  styleUrls: ['./buy-form.component.css']
})
export class BuyFormComponent implements OnInit {
  
  @Input()
  product: any;
  
  response: any;

  order: any;

  baseMoney = 100;
  limit = 1000;
  money: number = 0;
  buyForm : FormGroup;
  moneySub;
  // constructor(private bf: FormBuilder) {
  //     this.buyForm = this.bf.group({
  //       quantity: [1],
  //     });
  //     this.moneySub = this.buyForm.get("quantity")?.valueChanges
  //     .subscribe(value => this.money = value*this.baseMoney);
  //  }

   constructor(private bf: FormBuilder, private productService:ProductService, private singleOrderService: SingleOrderService, private router: Router) {
    this.buyForm = this.bf.group({
      quantity: [0],
    });
    this.moneySub = this.buyForm.get("quantity")?.valueChanges
    .subscribe(value => this.money = value*this.product.price);
 }
  ngOnInit(): void {
  }


  addToCart(){
    console.log("ready to add to cart");
    this.productService.addProductToCart(this.product, this.buyForm.get("quantity")?.value)
    .subscribe((res) =>{
      
    });
  }


  clickToBuy(){
   this.order = {"total":this.money, "payment":"Amex",
    "orderItems":[{"product":this.product, "qty":this.buyForm.get("quantity")?.value}]};
   console.log(this.product);
    this.singleOrderService.singleProductOrder(this.order);
  }
}
