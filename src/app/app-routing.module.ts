import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './demos/login/login.component';
import { LogoutComponent } from './demos/logout/logout.component';
import { OrderComponent } from './demos/order/order.component';
import { ProductComponent } from './demos/product/product.component';
import { RegisterComponent } from './demos/register/register.component';
import { ShoppingCartComponent } from './demos/shopping-cart/shopping-cart.component';
import { ShoppingPageComponent } from './demos/shopping-page/shopping-page.component';
import { AdminDashboardComponent } from './demos/admin-dashboard/admin-dashboard.component';
import { AddProductComponent } from './demos/admin-dashboard/add-product/add-product.component';
import { EditProductComponent } from './demos/admin-dashboard/edit-product/edit-product.component';
import { DeleteProductComponent } from './demos/admin-dashboard/delete-product/delete-product.component';
import { CheckOrderComponent } from './demos/admin-dashboard/check-order/check-order.component';
import { AppGuard } from './app.guard';

const routes: Routes = [
  {
    path: '',
      redirectTo: 'shoppage',
      pathMatch: 'full',
  },
  {
  path:'product',
  component: ProductComponent
},
{
  path:'product/:id',
  component: ProductComponent
},
{
  path:'shoppage',
  component: ShoppingPageComponent
},
{
  path:'order',
  component: OrderComponent
},
{
  path:'login',
  component: LoginComponent
},
{
  path: 'register',
    component: RegisterComponent
},
{
  path: '',
  canActivate: [AppGuard],
  children:[
    {
      path:'shopcart',
      component: ShoppingCartComponent
    },
    {
      path: 'logout',
      component: LogoutComponent
    },
    {
      path: 'manageDashboard',
      component: AdminDashboardComponent
    },
    {
      path: 'adminAddProduct',
      component: AddProductComponent
    },
    {
      path: 'adminEditProduct',
      component: EditProductComponent
    },
    {
      path: 'adminDeleteProduct',
      component: DeleteProductComponent
    },
    {
      path: 'adminCheckOrder',
      component: CheckOrderComponent
    },
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
